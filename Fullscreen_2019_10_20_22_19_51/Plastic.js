class Plastic{
   constructor(d, s, locationx, locationy ) {
     this.direction = d;
     this.speed = s;
     this.x = locationx;
     this.y = locationy;
   }
  
  update() {
     this.x = this.x + this.speed * Math.cos(this.direction);
     this.y = this.y + this.speed * Math.sin(this.direction);
     this.direction = Math.random() * Math.PI * 2;
  }
  
  border(w, h) {
    if (this.x >= w) {
      if (Math.random() > 0.7 ) {
        this.reset(w, h);
      }
      this.x = 0;
    }
    else if (this.y >= h) {
      if (Math.random() > 0.7 ) {
        this.reset(w, h);
      }
      this.y = 0;
    }
    else if (this.x <= 0) {
      if (Math.random() > 0.7 ) {
        this.reset(w, h);
      }
      this.x = w;
    }
    else if (this.y <= 0 ) {
      if (Math.random() > 0.7 ) {
        this.reset(w, h);
      }
      this.y = h;
    }
  }
  
  current( flow ) {
    this.direction = (this.direction + flow) / 2;
  }
  
  reset(w, h) {
    this.x = Math.random() * w;
    this.y = Math.random() * h;
    this.direction = Math.random() * Math.PI * 2;
  }
}