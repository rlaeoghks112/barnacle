class Boat{
 constructor(d, s, locationx, locationy ) {
   this.direction = d;
   this.speed = s;
   this.x = locationx;
   this.y = locationy;
   this.plastics = 0;
   this.red = 0;
 }
  
  update() {
   this.x = this.x + this.speed * Math.cos(this.direction);
   this.y = this.y + this.speed * Math.sin(this.direction);
  }
  
  reset() {
    this.direction = Math.random() * Math.PI * 2;
    this.plastics = 0;
    this.red = 0;
  }
  
  border(w, h) {
    if (this.x > w) {
      this.x = 0;
      this.reset();
    }
    else if (this.y > h) {
      this.y = 0;
      this.reset();
    }
    else if (this.x < 0) {
      this.x = w;
      this.reset();
    }
    else if (this.y < 0 ) {
      this.y = h;
      this.reset();
    }
  }
  
  collect() {
    this.plastics ++;
    if (this.red < 248) {
      this.red += 7;
    }
  }
}