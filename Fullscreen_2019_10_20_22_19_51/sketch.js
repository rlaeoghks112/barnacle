let width = 1520;
let height = 600;
let scl = 20;
let cols = width / scl;
let rows = height / scl;
let current = new Array(cols);
let numplastics = 1100;
let numboats = 1;
let boatspeed = 2;
let total = 0;
let graph = new Array(width);
let time = width;
let graphsize = 1;

for ( i = 0 ; i < rows ; i ++ ) {
  current[i] = new Array(cols);
}

for ( i = 0 ; i < width ; i ++ ) {
  graph[i] = 0;
}


let boat = new Boat(Math.random() * Math.PI * 2, boatspeed, Math.random() * width, Math.random() * height);
//Boat(direction, speed, x, y);
let plastics = new Array(numplastics);
let boats = new Array(100);

for ( i = 0 ; i < numplastics ; i ++ ) {
  plastics[i] = new Plastic( Math.random() * Math.PI * 2, 0.8, Math.random() * width, Math.random() * height);
}
for ( i = 0 ; i < numplastics ; i ++ ) {
  boats[i] = new Boat( Math.random() * Math.PI * 2, boatspeed, Math.random() * width, Math.random() * height);
}
for ( i = 0 ; i < rows ; i ++ ) {
  for ( j = 0 ; j < cols ; j ++ ) {
    current[i][j] = Math.random() * Math.PI * 2;
  }
}

function setup() {
  createCanvas(width, height + 200);
  var yoff = 0;
  for ( i = 0 ; i < rows ; i ++ ) {
    var xoff = 0;
    for ( j = 0 ; j < cols ; j ++ ) {
      current[i][j] = map(noise(xoff, yoff), 0, 1, -Math.PI*2, Math.PI*2);
      xoff += 0.06;
    }
    yoff += 0.06;
  }
}

function draw() {
  background(0);
  noStroke();
  for ( i = 0 ; i < numboats ; i ++ ) {
    fill(boats[i].red, 0, 255 - boats[i].red);
    arc(boats[i].x, boats[i].y, 10, 10, 0, 2 * Math.PI);
    boats[i].update();
    textSize(17);
    text(boats[i].plastics, boats[i].x + 10, boats[i].y + 10);
    boats[i].border(width, height);
  }
  
  fill(255);
  textSize(20);
  text("Total collected: " + total, 10, 20);
  text("Total collected", width - 200, height + 30);
  text("Time: " + (int) (millis()/1000) + "s", 10, height + 30);
  text("Number of ships: " + numboats, 10, 40);
  graph[width-1] = total/2;
  text(total, width - 50, height + 180 - graph[width-1]/graphsize);
  stroke(255);
  for ( i = 1 ; i < width ; i ++ ) {
    point( i, height + 200 - graph[i]/graphsize);
    graph[i-1] = graph[i];
  }
  print(graphsize);
  if ( total / graphsize > 350 ) {
    graphsize ++;
  }
  noStroke();
    
  
  for ( i = 0 ; i < numplastics ; i ++ ) {
    rect(plastics[i].x, plastics[i].y, 2, 2);
    plastics[i].update();
    plastics[i].border(width, height);
    for ( j = 0 ; j < numboats ; j ++ ) {
      if (plastics[i].x > boats[j].x - 5 && plastics[i].x < boats[j].x + 5 && plastics[i].y > boats[j].y - 5 && plastics[i].y < boats[j].y + 5) {
        boats[j].collect();
        plastics[i].reset(width, height);
        total ++;
      }
    }
  }
  for ( i = 0 ; i < rows ; i ++ ) {
    for ( j = 0 ; j < cols ; j ++ ) {
      for ( n = 0 ; n < numplastics ; n ++ ) {
        var x = plastics[n].x;
        var y = plastics[n].y;
        if ( x > j * scl && x < (j+1) * scl && y > i * scl && y < (i+1) * scl ) {
          plastics[n].current( current[i][j] );
        }
      }
    }
  }
  
}

function mouseReleased() {
  numboats ++;
  boats[numboats-1] = new Boat(Math.random() * Math.PI * 2, boatspeed, mouseX, mouseY);
}